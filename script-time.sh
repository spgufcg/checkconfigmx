pasta_tempos="$1"

file1=$1/"tempos-diff.txt"
file2=$1/"tempos-linhas.txt"
file3=$1/"tempos-macros.txt"
file4=$1/"tempos-filtro.txt"
file5=$1/"tempos-categorizacao.txt"


format_number (){
    IFS='m'
    ary=($1)
    minutos="${ary[0]}"
    segundos="${ary[1]}"
    segundos="0${segundos/s/}"
    resultado=$(($minutos * 60))
    resultado=$(python -c "print $resultado + $segundos")
    echo $resultado
}

paste $file1 $file2 $file3 $file4 $file5 | while IFS="$(printf '\t')" read -r f1 f2 f3 f4 f5
do
diff=$(format_number $f1)
linhas=$(format_number $f2)
macros=$(format_number $f3)
filtro=$(format_number $f4)
categ=$(format_number $f5)

total=$(python -c "print $diff + $linhas + $macros + $filtro + $categ")

step1=$(python -c "print $diff + $linhas")

echo $step1 $macros $filtro $categ $total >> $1/tempos.txt
done
