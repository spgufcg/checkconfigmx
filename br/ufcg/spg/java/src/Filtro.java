import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Filtro {
    ArrayList<String> warningAntes;
    ArrayList<String> noteAntes;
    ArrayList<String> errorAntes;
    
    ArrayList<String> warningDepois;
    ArrayList<String> noteDepois;
    ArrayList<String> errorDepois;
    
    private String path_logs;
    private String path_filtro;
    private String path_warnings;
    private String par;
    private String fileName;
    
    private Map<String, Integer> warningsMap = new HashMap<String, Integer>();
    
    public static void main(String[] args) throws IOException {
        //        		Filtro filtro = new Filtro("/Users/larissa/Dropbox/Experimento_LPS/exp-modutils",
        //        				"logs", "filtro", "e1de3af-f439304");
        Filtro filtro = new Filtro(args[0], args[1], args[2], args[3], args[4]);
        filtro.start();
    }
    
    public Filtro(String path_exp, String path_logs, String path_filtro, String par, String fileName){
        this.path_logs = path_exp + "/" + path_logs;
        this.path_filtro = path_exp + "/" + path_filtro;
        this.path_warnings = path_exp + "/warnings";
        this.par = par;
        this.fileName = fileName.split("/")[0];
    }
    
    private void start() throws IOException {
        String path_logs_par = path_logs + "/" + par;
        String antes = par.split("-")[0];
        String depois = par.split("-")[1];
        
        File folder = new File(path_logs_par);
        File[] listOfFiles = folder.listFiles();
        
        String dadosLog = "";
        String dadosFiltro = "";
        
        for (File file : listOfFiles) {
            if (file.isFile() && file.getName().startsWith(antes)) {
                String fileAntes = path_logs_par + "/" + file.getName();
                String fileDepois = path_logs_par + "/" + file.getName().replace(antes, depois);
                String versao = file.getName().replace(antes + "-", "").replace(".txt", "");
                String filtroErros = path_filtro + "/" + par + "/" + versao
                + "erros.txt";
                String filtroAvisos = path_filtro + "/" + par + "/" + versao
                + "avisos.txt";
                String filtroNotas = path_filtro + "/" + par + "/" + versao
                + "notas.txt";
                
                String aux = process(antes, depois, fileAntes, fileDepois, filtroErros,
                                     filtroAvisos, filtroNotas);
                
                dadosLog += versao + " " + aux.split("\\..")[0]  + "\n";
                dadosFiltro += versao + " " + aux.split("\\..")[1] + "\n";
            }
        }
        
        FileWriter writer = new FileWriter(path_logs + "/" + par + ".txt", false);
        writer.write(dadosLog);
        writer.close();
        
        FileWriter w = new FileWriter(path_filtro + "/" + par + ".txt", false);
        w.write(dadosFiltro);
        w.close();
        
        
        if(!warningsMap.isEmpty()){
            FileWriter m = new FileWriter(path_warnings + "/" + par + "-warningMap.txt", false);
            String dadosWarningMap = "";
            for (String k : warningsMap.keySet()) {
                dadosWarningMap += k + "   |||   " + warningsMap.get(k) + "\n";
            }
            m.write(dadosWarningMap);
            m.close();
        }
    }
    
    private String process(String antes, String depois, String fileAntes, String fileDepois, String filtroErros,
                           String filtroAvisos, String filtroNotas) throws IOException {
        warningAntes = new ArrayList<String>();
        noteAntes = new ArrayList<String>();
        errorAntes = new ArrayList<String>();
        
        warningDepois = new ArrayList<String>();
        noteDepois= new ArrayList<String>();
        errorDepois = new ArrayList<String>();
        
        processAntes(antes, fileAntes);
        processDepois(depois, fileDepois);
        
        String dados = "";
        dados += warningAntes.size() + " ";
        dados += errorAntes.size() + " ";
        dados += noteAntes.size() + " ";
        
        dados += warningDepois.size() + " ";
        dados += errorDepois.size() + " ";
        dados += noteDepois.size() + " ";
        
        ArrayList<String> warning= new ArrayList<String>();
        ArrayList<String> note = new ArrayList<String>();
        ArrayList<String> error = new ArrayList<String>();
        
        for (String w : warningDepois) {
            if(warningAntes.contains(w))
                warningAntes.remove(w);
            else{
                warning.add(w);
            }
        }
        
        for (String w : errorDepois) {
            if(errorAntes.contains(w))
                errorAntes.remove(w);
            else{
                error.add(w);
            }
        }
        
        for (String w : noteDepois) {
            if(noteAntes.contains(w))
                noteAntes.remove(w);
            else{
                note.add(w);
            }
        }
        
        warningsMap(warning);
        
        writeOnFile(filtroErros, error);
        writeOnFile(filtroAvisos, warning);
        writeOnFile(filtroNotas, note);
        
        dados += ".." + warning.size() + " " + note.size() + " " + error.size();
        
        return dados;
        
    }
    
    private void warningsMap(ArrayList<String> warning) {
        for (String w : warning) {
            if(!warningsMap.containsKey(w)){
                warningsMap.put(w, 1);
            }else{
                warningsMap.computeIfPresent(w, (k, v) -> v + 1);
            }
        }
    }
    
    private void writeOnFile(String file, ArrayList<String> lista) throws IOException {
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "utf-8"));
        for (String l : lista) {
            writer.write("\n\n");
            writer.write(l);
        }
        
        writer.close();
    }
    
    @SuppressWarnings("resource")
    private void processAntes(String antes, String fileAntes) throws IOException {
        BufferedReader br = null;
        
        String sCurrentLine;
        String linha = "";
        
        br = new BufferedReader(new FileReader(fileAntes));
        
        while ((sCurrentLine = br.readLine()) != null) {
            if(sCurrentLine.startsWith("//#$")){
                //do nothing
            }
            if(sCurrentLine.startsWith("/Volumes/SAMSUNG/")){
                addLinhaAntes(antes, linha);
                linha = "" + sCurrentLine.trim();
            }else{
                linha = linha + "\n " + sCurrentLine.trim();
            }
        }
    }
    
    @SuppressWarnings("resource")
    private void processDepois(String depois, String fileDepois) throws IOException {
        BufferedReader br = null;
        
        String sCurrentLine;
        String linha = "";
        
        br = new BufferedReader(new FileReader(fileDepois));
        
        while ((sCurrentLine = br.readLine()) != null) {
            if(sCurrentLine.startsWith("//#$")){
                //do nothing
            }
            if(sCurrentLine.startsWith("/Volumes/SAMSUNG/")){
                addLinhaDepois(depois, linha);
                linha = "" + sCurrentLine.trim();
            }else{
                linha = linha + "\n " + sCurrentLine.trim();
            }
        }
    }
    
    public void addLinhaAntes(String antes, String linha) {
        System.out.println(this.fileName);
        System.out.println(linha);
        System.out.println(linha.contains(fileName));
        if(linha.contains(this.fileName)){
            linha = linha.replace(antes, "");
            if(linha.contains("error:")){
                String message = linha.split("error:")[1];
                errorAntes.add(message);
            }
            
            if(linha.contains("warning:")){
                String message = linha.split("warning:")[1];
                warningAntes.add(message);
            }
            
            if(linha.contains("note:")){
                String message = linha.split("note:")[1];
                noteAntes.add(message);
            }
        }
    }
    
    public void addLinhaDepois(String depois, String linha) {
        if(linha.contains(this.fileName)){
            linha = linha.replace(depois, "");
            if(linha.contains("error:")){
                String message = linha.split("error:")[1];
                errorDepois.add(message);
            }
            
            if(linha.contains("warning:")){
                String message = linha.split("warning:")[1];
                warningDepois.add(message);
            }
            
            if(linha.contains("note:")){
                String message = linha.split("note:")[1];
                noteDepois.add(message);
            }
        }
    }
}
