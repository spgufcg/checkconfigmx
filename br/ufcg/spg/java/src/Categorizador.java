import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class Categorizador {
    
    public static String pathToFilters;
    private static String path;
    private static Map<String, String> errors;
    private static String resultadoCategorizacao;
    private static String resultadoCategorizacaoPerFile;
    
    public static void main(String[] args) throws IOException {
        Categorizador.resultadoCategorizacao = "";
        
        Categorizador.path = args[0];
        Categorizador.pathToFilters = Categorizador.path + "/" + args[1];
        
        Categorizador.groupErrors();
        
    }
    
    public static void groupErrors() throws IOException{
        
        File folder = new File(pathToFilters);
        groupErrors(folder);
    }
    
    private static void groupErrors(File directory) throws IOException {
        errors = new HashMap<String, String>();
        //		System.out.println(directory.getAbsolutePath());
        for (File file : directory.listFiles()) {
            //			System.out.println(file);
            if(file.getName().contains("erros")){
                readFile(file);
            }
        }
        
        System.out.println(errors.size());
        if(errors.size() > 0){
            FileWriter writer = new FileWriter(path + "/" + "categorization" + ".txt", true);
            writer.write(directory.getName() + " " + errors.size() + "\n");
            writer.close();
            writeCategorizationLog(directory);
        }
    }
    
    private static void writeCategorizationLog(File directory)
    throws IOException {
        resultadoCategorizacao += directory.getName() + " " + errors.size() + "\n";
        resultadoCategorizacaoPerFile = "";
        resultadoCategorizacaoPerFile += "----"  + "\n";
        resultadoCategorizacaoPerFile += "ERROS DO PAR: " + directory.getName()  + "\n";
        resultadoCategorizacaoPerFile += "\n"  + "\n";
        resultadoCategorizacaoPerFile += "erro ### configs"  + "\n";
        for (String erro : errors.keySet()) {
            resultadoCategorizacaoPerFile += erro + "###" + errors.get(erro)  + "\n";
        }
        
        File file = new File(path + "/categorization/" + directory.getName() + ".txt");
        file.getParentFile().mkdirs();
        FileWriter writer = new FileWriter(file, true);
        writer.write(resultadoCategorizacaoPerFile);
        writer.close();
    }
    
    private static void readFile(File file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            
            String fileTxtContent = "";
            
            
            
            while ((line = br.readLine()) != null) {
                if(line.trim().isEmpty()){
                    line = "###";
                }
                fileTxtContent += line;
            }
            
            String[] content = fileTxtContent.split("###");
            
            for (int i = 0; i < content.length; i++) {
                String errorMessage = getErrorMessage(content, i);
                if(!errorMessage.trim().isEmpty()){
                    String config = file.getName().split("erros")[0];
                    if(checkIfExists(errorMessage)){
                        System.out.println(errorMessage);
                        errors.put(errorMessage, config);
                    }else{
                        String configs = errors.get(errorMessage);
                        configs = configs + " " + config;
                        errors.put(errorMessage, configs);
                    }
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    private static String getErrorMessage(String[] content, int i) {
        String completeErrorMessage = content[i].replace("~", "").replace("^", "");
        
        String simplifiedErrorMessage = "";
        
        if(completeErrorMessage.contains("use of undeclared identifier") ||
           completeErrorMessage.contains("incomplete definition of type")){
            String[] aux = completeErrorMessage.split("'");
            simplifiedErrorMessage = aux[0] + aux [1];
            return simplifiedErrorMessage;
        }
        
        if(completeErrorMessage.contains("no member named")){
            String[] aux = completeErrorMessage.split("'");
            simplifiedErrorMessage = aux[0] + aux [1] + aux[2] + aux[3];
            return simplifiedErrorMessage;
        }
        
        if(completeErrorMessage.contains("use of undeclared identifier")){
            String[] aux = completeErrorMessage.split("'");
            simplifiedErrorMessage = aux[0] + aux [1];
            return simplifiedErrorMessage;
        }
        
        return completeErrorMessage;
    }
    
    private static boolean checkIfExists(String errorMessage) {
        return !errors.keySet().contains(errorMessage);
    }
}
