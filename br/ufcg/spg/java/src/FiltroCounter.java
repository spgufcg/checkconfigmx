import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.commons.io.FileUtils;

public class FiltroCounter {
    private static String pasta_filtro;
    private static String path;
    
    public static void main(String[] args) {
        //		String path = "/Volumes/SAMSUNG/apache/core/";
        
        path = args[0];
        pasta_filtro = args[1];
        
        ArrayList<String> pares = getPares(path);
        
        for (String par : pares) {
            getValoresFiltro(par, path);
        }
    }
    
    private static void getValoresFiltro(String par, String path) {
        BufferedReader br = null;
        
        try {
            
            String sCurrentLine;
            
            br = new BufferedReader(new FileReader(path
                                                   + pasta_filtro + "/" + par + ".txt"));
            
            boolean entrou = false;
            int nota = 0;
            int aviso = 0;
            int erro = 0;
            int configsDiffZero = 0;
            int totalConfigs = 0;
            while ((sCurrentLine = br.readLine()) != null) {
                entrou = true;
                totalConfigs++;
                String[] aux = sCurrentLine.split(" ");
                aviso += Integer.parseInt(aux[1]);
                nota += Integer.parseInt(aux[2]);
                erro += Integer.parseInt(aux[3]);
                if (Integer.parseInt(aux[3]) > 0)
                    configsDiffZero++;
            }
            if(entrou){
                String linha = par + " " + totalConfigs
                + " " + configsDiffZero + " " + aviso + " " + nota + " " + erro + "\n";
                
                writeOnFile(linha);
                //				copyRelevantCommitsFolders(par, erro);
            }
            else{
                String linha = par + " " + "- - - - - \n";
                writeOnFile(linha);
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
    }
    
    private static void writeOnFile(String linha) throws IOException {
        
        
        FileWriter writer = new FileWriter(path + "/" + "filtro-counter" + ".txt", true);
        writer.write(linha);
        writer.close();
    }
    
    private static ArrayList<String> getPares(String path) {
        ArrayList<String> pares = new ArrayList<String>();
        
        BufferedReader br = null;
        
        try {
            
            String sCurrentLine;
            
            br = new BufferedReader(new FileReader(path
                                                   + "dados-macros-generation.txt"));
            
            while ((sCurrentLine = br.readLine()) != null) {
                String[] aux = sCurrentLine.split(",");
                pares.add(aux[0] + "-" + aux[1]);
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return pares;
    }
}
