import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class CategorizadorWarning {

	private String path;

	private ArrayList<String> content;
	
	private String pair = "";
	
	private String totalWarningsPath = "";

	public static void main(String[] args) throws IOException {
		CategorizadorWarning cw = new CategorizadorWarning();

		cw.pair = args[1];
		
		cw.totalWarningsPath = args[0].split("warnings")[0] + "categorization-warnings.txt";
		
		cw.path = args[0] + args[1] + "-warningMap.txt";

		cw.groupErrors();

	}


	private void groupErrors() throws IOException {

		File wFile = new File(this.path);
		
		readFile(wFile);
		
		rewriteFile(wFile);
		
		updateTotalOfWarnings();
	}

	private void updateTotalOfWarnings() {
		File totalWarningsFile = new File(this.totalWarningsPath);
		
		writeFile(totalWarningsFile, true, this.pair + " " + content.size() + "\n");         
		
		System.out.println(totalWarningsPath);
	}


	private void writeFile(File file, boolean append, String text) {
		FileWriter fWriter;

		try {
		    
		    fWriter = new FileWriter(file, append);
		    fWriter.write(text);

		    fWriter.close();
		 } catch (IOException e) {
		       e.printStackTrace();
		 }
	}


	private void rewriteFile(File wFile) {
		String source = "";
		
		for (String warning : content) {
			source += warning + "\n";
		}
		
		writeFile(wFile, false, source);          
	}


	private void readFile(File file) {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;

			content = new ArrayList<String> ();

			while ((line = br.readLine()) != null) {
				if(!line.trim().isEmpty() && !line.contains("making: not found") && !line.contains("Is a directory")){
					line = line.split("\\|\\|\\|")[0];
					if(!content.contains(line)) {
						content.add(line);
					}
				}
			}


		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}