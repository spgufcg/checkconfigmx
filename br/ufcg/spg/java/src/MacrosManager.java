import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class MacrosManager {
    private ArrayList<String> macros_impactadas;
    private ArrayList<String> macros_file;
    private ArrayList<String> macros_format;
    private ArrayList<String> macros_comb;
    private ArrayList<Integer> linhas_start;
    private ArrayList<Integer> linhas_end;
    private ArrayList<Integer> linhas_changed;
    private String COMANDO_GCC = "gcc -ferror-limit=0 ";
    private String path_exp;
    private String path_commits;
    private String projeto;
    private String fileName;
    private String pasta_gcc;
    private String pasta_log;
    private String path_file;
    private String path_linhas;
    
    public static void main(String[] args) throws Exception {
        
        MacrosManager m = new MacrosManager(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
        
        //                 MacrosManager m = new MacrosManager("busybox",
        //                		 "/Users/larissa/Dropbox/Experimento_LPS/exp-ash",
        //                "98c5264-0dfe1d2.txt", "gcc",
        //                "commits", "logs", "shell/ash.c", "linhas");
        
        
        m.start();
        
        //close the file (VERY IMPORTANT!)
    }
    
    public MacrosManager(String projeto, String path_exp, String fileName, String pasta_gcc,
                         String pasta_commits, String pasta_log, String path_file, String path_linhas){
        this.projeto = projeto;
        this.path_exp = path_exp;
        this.fileName = fileName;
        this.pasta_gcc = path_exp + "/"+ pasta_gcc;
        this.path_commits = path_exp + "/" + pasta_commits;
        this.pasta_log = path_exp + "/"+ pasta_log;
        this.path_file = path_file;
        this.path_linhas = path_linhas;
    }
    
    
    private void start() throws Exception {
        fileName = fileName.replace(".txt", "");
        findMacrosLines(fileName);
        findChangedLines(fileName);
        getChangedMacros();
        getMacrosFormat();
        if(macros_impactadas.size() <= 4){
//            getMacrosCombination();
//            getGCCconfigs(fileName.replace("/", ""), pasta_gcc);
        } else{
        	getLsaSamplingLSA();
            getGCCconfigsLSA(fileName.replace("/", ""), pasta_gcc);
        }
        String[] aux = fileName.split("-");
        String results = (aux[0] + ","  + aux[1] +
                          ","+ linhas_changed.size() +
                          ","+ macros_impactadas.size() +
                          "," + macros_format.size()) + "\n";
        FileWriter writer = new FileWriter(path_exp + "/dados-macros-generation.txt", true);
        writer.write(results);
        writer.close();
    }
    
    private void getLsaSamplingLSA() {
        macros_comb = new ArrayList<String>();
        
        //one-enabled
        macros_comb.addAll(macros_impactadas);
        
        //one-disabled
        ArrayList<String> combOneDisabled = getOneDisabled(macros_impactadas);
        for (String comb : combOneDisabled) {
            if(!macros_comb.contains(comb.trim()))
                macros_comb.add(comb.trim());
        }
        
        //all enabled
        String config = getAllEnabled(macros_impactadas);
        if(!config.isEmpty() && !macros_comb.contains(config.trim()))
            macros_comb.add(config.trim());
    }
    
    private ArrayList<String> getOneDisabled(
                                             ArrayList<String> macros_impactadas) {
        ArrayList<String> oneDisabledList = new ArrayList<String>();
        for (String macro1 : macros_impactadas) {
            String config = "";
            for (String macro2 : macros_impactadas) {
                if (!macro1.equals(macro2))
                    config += " " + macro2;
            }
            if(!config.isEmpty())
                oneDisabledList.add(config);
        }
        return oneDisabledList;
    }
    
    private String getAllEnabled(ArrayList<String> macros_impactadas) {
        String config = "";
        for (String macro : macros_impactadas) {
            config += " " + macro;
        }
        return config;
    }
    
    private void getGCCconfigsLSA(String file_name, String pasta_gcc) throws Exception {
        String log = pasta_gcc + "/" + file_name + ".txt";
        new File(log).createNewFile();
        String macros_enable= macros_format.toString().replace(", ", " -D").
        replace("[", "-D").replace("]", "").replace("-Dn ","-D");
        
        int log_versao = 0;
        String comandos = "";
        
        String antes = file_name.split("-")[0];
        String depois = file_name.split("-")[1];
        comandos += getGCCconfigsLSA(antes, log_versao, macros_enable, "");
        comandos += getGCCconfigsLSA(depois, log_versao, macros_enable, "");
        
        for (String m : macros_comb) {
            log_versao++;
            m = m.replace(" ", " -D");
            m = "-D" + m + " ";
            comandos += getGCCconfigsLSA(antes, log_versao, macros_enable, m);
            comandos += getGCCconfigsLSA(depois, log_versao, macros_enable, m);
        }
        
        System.out.println(comandos);
        
        String fileComandos = pasta_gcc + "/" + file_name + ".txt";
        
        System.out.println(fileComandos);
        PrintWriter writer = new PrintWriter(fileComandos, "UTF-8");
        writer.write(comandos);
        writer.close();
        
    }
    
    private String getGCCconfigsLSA(String versao, int log_versao, String macros_enable, String m_impact) throws Exception{
        String includes = " -I" + path_commits + "/" + projeto + "-" + versao + "/include ";
        String comando_enabled = COMANDO_GCC + includes + m_impact + macros_enable + " " + path_commits + "/"
        + projeto + "-" + versao
        + "/*/*.c" + " 2> " + pasta_log + "/" + fileName + "/" + versao + "-e" + log_versao + ".txt";
        String comando_disabled = COMANDO_GCC + includes + m_impact + " " + path_commits + "/"
        + projeto + "-" + versao
        + "/*/*.c" + " 2> " + pasta_log + "/" + fileName + "/" + versao + "-d" + log_versao + ".txt";
        
        runGCC(comando_disabled);
        runGCC(comando_enabled);
        
        return (comando_disabled + "\n\n" + comando_enabled + "\n\n").replace("  ", " ");
    }
    
    private void getGCCconfigs(String file_name, String pasta_gcc) throws Exception {
        String log = pasta_gcc + "/" + file_name + ".txt";
        new File(log).createNewFile();
        String macros_enable= macros_format.toString().replace(", ", " -D").
        replace("[", "-D").replace("]", "").replace("-Dn ","-D");
        
        int log_versao = 0;
        String comandos = "";
        
        String antes = file_name.split("-")[0];
        String depois = file_name.split("-")[1];
        comandos += getGCCconfigs(antes, log_versao, macros_enable, "");
        comandos += getGCCconfigs(depois, log_versao, macros_enable, "");
        
        for (String m : macros_comb) {
            log_versao++;
            m = m.replace(" ", " -D");
            if(!m.equals("-D")){
                m = "-D" + m + " ";
                comandos += getGCCconfigs(antes, log_versao, macros_enable, m);
                comandos += getGCCconfigs(depois, log_versao, macros_enable, m);
                comandos = comandos.replace(" -D ", " ");
                comandos = comandos.replace(" ", " ");
            }
        }
        
        String fileComandos = pasta_gcc + "/" + file_name + ".txt";
        PrintWriter writer = new PrintWriter(fileComandos, "UTF-8");
        writer.write(comandos);
        writer.close();
        
        
    }
    
    private String getGCCconfigs(String versao, int log_versao, String macros_enable, String m_impact) throws Exception{
        String includes = " -I" + path_commits + "/" + projeto + "-" + versao + "/include ";
        String comando_enabled = COMANDO_GCC + includes + m_impact + macros_enable + " " + path_commits + "/"
        + projeto + "-" + versao
        + "/" + path_file + " 2> " + pasta_log + "/" + fileName + "/" + versao + "-e" + log_versao + ".txt";
        String comando_disabled = COMANDO_GCC + includes + m_impact + " " + path_commits + "/"
        + projeto + "-" + versao
        + "/" + path_file + " 2> " + pasta_log + "/" + fileName + "/" + versao + "-d" + log_versao + ".txt";
        
        comando_disabled = comando_disabled.replace(" -D ", " ");
        comando_disabled = comando_disabled.replace(" ", " ");
        
        comando_enabled = comando_enabled.replace(" -D ", " ");
        comando_enabled = comando_enabled.replace(" ", " ");
        
        runGCC(comando_disabled);
        runGCC(comando_enabled);
        
        return (comando_disabled + "\n\n" + comando_enabled + "\n\n").replace("  ", " ");
    }
    
    private void runGCC(String comando)
    throws FileNotFoundException, UnsupportedEncodingException, IOException, InterruptedException {
        PrintWriter writer = new PrintWriter("runGCC.sh", "UTF-8");
        writer.println(comando.replaceAll("  ", " "));
        writer.close();
        
        Runtime rt = Runtime.getRuntime();
        Process p = rt.exec("sh runGCC.sh");
        p.waitFor();
    }
    
    private void getMacrosCombination() {
        macros_comb = new ArrayList<String>();
        CombinationGenerater cg = new CombinationGenerater();
        macros_comb = cg.generateMacrosCommand(macros_impactadas);
    }
    
    private void getMacrosFormat() {
        macros_format = new ArrayList<String>();
        for (String m : macros_file) {
            String macro = m.replace("!", "").replace("  ", " ").replaceAll("\\//*.*\\*/", "");
            if(!(macro.contains("=")) && !(macro.contains("<")) && !(macro.contains(">"))
               && !(macro.contains("defined")) && !(macro.contains("this_way")) && !macro.contains("ined")){
                macro = macro.replace("#if", "").replace("ndef","").replace("def","").replace("#ifndef","").replace("#ifdef ", "").replace("|", " ").replace("  "," ");
                String[] macros = macro.split("\\ ");
                for (int j = 0; j < macros.length; j++) {
                    if(!macros_format.contains(macros[j].trim()) && !macros[j].isEmpty()
                       && !macros[j].trim().contains("0 ") && !macros[j].trim().equals("0")
                       && !macros[j].trim().equals("1") && !macros[j].trim().equals("BB_VER")){
                        if(!macros[j].contains("&&"))
                            macros_format.add(macros[j].trim());
                        else{
                            macros = macros[j].split("&&");
                            for (int i = 0; i < macros.length; i++) {
                                if(!macros_format.contains(macros[i].trim()) && !macros[i].isEmpty()
                                   && !macros[j].trim().contains("0 ") && !macros[j].trim().equals("0") &&
                                   !macros[j].contains("ined") && !macros[j].trim().equals("1")
                                   && !macros[j].trim().equals("BB_VER")){
                                    if(!macros[j].contains("&&")){
                                        macros_format.add(macros[i].trim());
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
        }
        for (String m : macros_impactadas) {
            if (macros_format.contains(m))
                macros_format.remove(m);
        }
    }
    
    private void getChangedMacros() {
        macros_impactadas = new ArrayList<String>();
        for (Integer line : linhas_changed) {
            for (int i = 0; i < linhas_start.size(); i++) {
                if(linhas_start.get(i) <= line && linhas_end.get(i) >= line){
                    String macro = macros_file.get(i).replace("!", "").replace("#if", "").
                    replace("#ifdef ", "").replace("#ifndef","").replace("ndef","").replace("def","").replace("|", " ").replace("  ", " ").replaceAll("\\//*.*\\*/", "");
                    String[] macros = macro.split(" ");
                    for (int j = 0; j < macros.length; j++) {
                        if(!macros_impactadas.contains(macros[j].trim()) && !macros[j].isEmpty()){
                            if(!macros[j].contains("&&") && !macros[j].contains(">")
                               && !macros[j].contains("<") && !macros[j].contains("=")
                               && !macros[j].contains("defined") && !macros[j].contains("ined")
                               && !macro.contains("this_way") && !macros[j].trim().equals("1")
                               &&!macros[j].trim().equals("0") && !macros[j].trim().equals("BB_VER")){
                                macros_impactadas.add(macros[j].trim());
                            }
                            else{
                                macros = macros[j].split("&&");
                                for (int t = 0; t < macros.length; t++) {
                                    if(!macros_impactadas.contains(macros[t].trim()) && !macros[t].isEmpty()){
                                        if(!macros[t].contains("&&") && !macros[t].contains(">")
                                           && !macros[t].contains("<")	&& !macros[t].contains("<")
                                           && !macros[t].contains("=") && !macros[t].contains("defined")
                                           && !macros[t].contains("ined") && !macros[t].trim().equals("0")
                                           && !macros[t].trim().equals("1") && !macros[t].trim().equals("BB_VER")){
                                            macros_impactadas.add(macros[t].trim());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void findChangedLines(String lines_file_name) {
        linhas_changed = new ArrayList<Integer>();
        BufferedReader br = null;
        
        try {
            
            String arquivo_path = lines_file_name + ".txt";
            String line;
            
            br = new BufferedReader(new FileReader
                                    (path_exp + "/" + path_linhas + "/"
                                     + arquivo_path));
            
            while ((line = br.readLine()) != null) {
                if(!line.isEmpty()){
                    linhas_changed.add(Integer.parseInt(line.trim()));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    private void findMacrosLines(String lines_file_name) {
        macros_file = new ArrayList<String>();
        linhas_start = new ArrayList<Integer>();
        linhas_end = new ArrayList<Integer>();
        BufferedReader br = null;
        
        try {
            
            String arquivo_path = lines_file_name.split("-")[1]+ "/" + path_file;
            String line;
            
            br = new BufferedReader(new FileReader
                                    (path_commits + "/" + projeto + "-"
                                     + arquivo_path));
            
            //System.out.println(path_commits + "/" + projeto + "-"
              //                 + arquivo_path);
            
            int linha = 1;
            ArrayList<String> pilha_macros = new ArrayList<String>();
            ArrayList<Integer> pilha_linhas = new ArrayList<Integer>();
            
            
            while ((line = br.readLine()) != null) {
                //System.out.println(line);
                line = line.replace("# ", "#");
                if (line.trim().startsWith("#if")){
                    pilha_macros.add(line);
                    pilha_linhas.add(linha);
                }
                else if (line.trim().startsWith("#endif")){
                    if(!pilha_macros.isEmpty()){
                        int tamanho = pilha_macros.size();
                        String last_macro = pilha_macros.get(tamanho-1);
                        macros_file.add(last_macro);
                        linhas_start.add(pilha_linhas.get(tamanho-1));
                        linhas_end.add(linha);
                        pilha_macros.remove(tamanho-1);
                        pilha_linhas.remove(tamanho-1);
                    }
                }
                linha++;
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
