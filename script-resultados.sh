pasta_exp=$1
pasta_commits=$2
pasta_filtro=$3

line_count=0
commit_anterior=""

for line in $(cat "$pasta_exp${pasta_commits}.txt")
do
if !(( $line_count == 0 )) ; then
commit_atual="${line:0:7}"
pair="$commit_anterior"-"$commit_atual"
echo "${pasta_exp}" "${pasta_filtro}/${pair}"
(time ./script-categorizador.sh "${pasta_exp}" "${pasta_filtro}/${pair}") 2>&1 | grep real | awk '{print $2}' >> "${pasta_exp}tempos-categorizacao.txt"
fi
line_count=$(($line_count + 1))
commit_anterior="${line:0:7}"
done

./script-filtrocounter.sh ${pasta_exp} ${pasta_filtro}