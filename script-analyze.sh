projeto="$1"
pasta_exp="$2"
pasta_commits="$3"
pasta_diff="$4"
file_name="$5"
pasta_linhas="$6"
pasta_gcc="$7"
pasta_logs="$8"
pasta_filtro="$9"

mkdir "$pasta_diff"
mkdir "$pasta_linhas"
mkdir "$pasta_gcc"
mkdir "$pasta_logs"
mkdir "$pasta_filtro"
mkdir "$pasta_exp/warnings"

cd "$projeto"

#git reset --hard origin #/master #coloca o rep. no head
#git checkout HEAD

line_count=0
commit_anterior=""

for line in $(cat "$pasta_exp/${pasta_commits}.txt") 
do
	if !(( $line_count == 0 )) ; then
		commit_atual="${line:0:7}"
        echo "#analizando $commit_anterior $commit_atual"
		#diff
        arq_diff="$pasta_exp/$pasta_diff/"$commit_anterior"-"$commit_atual".txt"
 		.././script-diff.sh $commit_anterior $commit_atual $file_name $arq_diff
#linhas
		(time .././script-linhas.sh $pasta_linhas $pasta_diff $arq_diff) 2>&1 | grep real | awk '{print $2}' >> "../tempos-linhas.txt"
		#macros
		cd .. 
		mkdir "$pasta_logs/$commit_anterior-$commit_atual"
		(time ./script-macros.sh $projeto $pasta_exp "$commit_anterior-$commit_atual.txt" $pasta_gcc $pasta_commits $pasta_logs $file_name $pasta_linhas) 2>&1 | grep real | awk '{print $2}' >> "tempos-macros.txt"
        echo $projeto $pasta_exp "$commit_anterior-$commit_atual.txt" $pasta_gcc $pasta_commits $pasta_logs $file_name $pasta_linhas
		#filtro
        mkdir "$pasta_filtro/$commit_anterior-$commit_atual"
		(time ./script-filtro.sh $pasta_exp $pasta_logs $pasta_filtro "$commit_anterior-$commit_atual" $file_name) 2>&1 | grep real | awk '{print $2}' >> "tempos-filtro.txt"
		cd "$projeto"
	fi
	line_count=$(($line_count + 1))
	commit_anterior="${line:0:7}"
done

