pasta_exp=$1
pasta_commits=$2"-commits.txt"

line_count=0
commit_anterior=""

for line in $(cat "$pasta_exp${pasta_commits}")
do
if !(( $line_count == 0 )) ; then
commit_atual="${line:0:7}"
pair="$commit_anterior"-"$commit_atual"

echo "./script-warnings.sh" "${pasta_exp}warnings" "${pair}"

(time ./script-warnings.sh "${pasta_exp}warnings" "${pair}") 2>&1 | grep real | awk '{print $2}' >> "${pasta_exp}tempos-warning.txt"
fi
line_count=$(($line_count + 1))
commit_anterior="${line:0:7}"
done
