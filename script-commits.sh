nome="$1"
pasta_commits="$2"
pasta_exp="$3"
caminho_pasta="$4"
caminho_arquivo="$5"

mkdir "$pasta_commits"

echo "recuperando lista"

cd "$nome"

#git reset --hard origin/master #coloca o rep. no head

echo  "$caminho_arquivo"
git rev-list --reverse HEAD "$caminho_arquivo" > "$pasta_exp"/"$pasta_commits".txt

cd ..

echo "iniciando checkouts"

for line in $(cat "$pasta_commits".txt)
do
echo "$line"
cd "$nome"
git clean -dfx
#git reset --hard origin/master #coloca o rep. no head/Volumes/SAMSUNG/novo_experimento-gcc_global/busybox/README
git checkout $line -f #-- $caminho_pasta #altera o rep. para o commit da vez
#make defconfig -n
git show -s --format=%ci $line >> "$pasta_exp/datas.txt"
#svn co http://svn.apache.org/repos/asf/apr/apr/trunk srclib/apr
#./buildconf
#./configure
#make defconfig -n
#make include/bb_config.h -n
#echo "CONFIG_SED=y" >> .config -n
#echo "#undef ENABLE_SED" >> include/bb_config.h -n
#echo "#define ENABLE_SED 1" >> include/bb_config.h -n
#make -n
#mv busybox sed -n
cd ..
mkdir "$pasta_commits/$nome-${line:0:7}"
cp -a "$nome"/. "$pasta_commits/$nome-${line:0:7}"
done
